import React, { Component } from 'react'
import {Helmet} from "react-helmet/es/Helmet";
import Axios from "axios";



class Pages extends Component {
	constructor() {
		super();
		this.state = {
			meta_title: '',
			meta_keywords: '',
			meta_description: '',
			content: '',
			header: '',
			footer: ''
		}
	}

	componentDidMount() {
		let pageUrl = this.props.location.pathname;

		if(pageUrl === '/'){
			pageUrl = '/index'
		}

		if(pageUrl.substr(-1) === '/') {
			pageUrl = pageUrl.substr(0, pageUrl.length - 1);
		}

		try{
			if(localStorage.getItem('header') && localStorage.getItem('footer')){
				this.setState({
					header: localStorage.getItem('header'),
					footer: localStorage.getItem('footer')
				})
			}else{
				Axios.get('/config.json').then((result) => {
					localStorage.setItem('header', result.data.header);
					localStorage.setItem('footer', result.data.footer);
					this.setState({
						header: result.data.header,
						footer: result.data.footer
					})
				});
			}

			if(localStorage.getItem(`${pageUrl}-meta_title`) &&
				localStorage.getItem(`${pageUrl}-meta_keywords`) &&
				localStorage.getItem(`${pageUrl}-meta_keywords`) &&
				localStorage.getItem(`${pageUrl}-content`)
			){
				const page = localStorage.getItem(pageUrl);
				this.setState({
					meta_title: localStorage.getItem(`${pageUrl}-meta_title`),
					meta_keywords: localStorage.getItem(`${pageUrl}-meta_keywords`),
					meta_description: localStorage.getItem(`${pageUrl}-meta_description`),
					content: localStorage.getItem(`${pageUrl}-content`)
				})
			}else{
				Axios.get(`/pages/user${pageUrl}.json`).then((result) => {
					localStorage.setItem(`${pageUrl}-meta_title`, result.data.meta_title);
					localStorage.setItem(`${pageUrl}-meta_keywords`, result.data.meta_keywords);
					localStorage.setItem(`${pageUrl}-meta_description`, result.data.meta_description);
					localStorage.setItem(`${pageUrl}-content`, result.data.content);
					this.setState({
						meta_title: result.data.meta_title,
						meta_keywords: result.data.meta_keywords,
						meta_description: result.data.meta_description,
						content: result.data.content
					})
				})
			}
		}catch(e){
			this.setState({
				meta_title: '404',
				meta_keywords: '404',
				meta_description: '404',
				content: '404'
			})
		}
	}

	render(){
		return (
			<React.Fragment>
				<Helmet>
					<meta charSet="utf-8" />
					<title>{this.state.meta_title}</title>
					<meta name="keywords" content={this.state.meta_keywords} />
					<meta name="description" content={this.state.meta_description} />
				</Helmet>
				{require('html-react-parser')(
						`${this.state.header}${this.state.content}${this.state.footer}`
				)}
			</React.Fragment>
		);
	}
}

export default Pages;