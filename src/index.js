import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Pages from './controllers/pages/Pages'
// import Login from './controllers/user/Login'
// import Invoices from './controllers/user/Invoices'
import ReactDOM from "react-dom";

function App() {
	return (
		<Router>
			<Route path="*" component={Pages} />
		</Router>
	);
}

ReactDOM.render(<App/>, document.getElementById('root'));
