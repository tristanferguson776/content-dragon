ContentBox.js ver. 4.0.3


*** USAGE ***

1. Include the required css:

    <link href="box/box.css" rel="stylesheet" type="text/css" />
	<link href="assets/minimalist-blocks/content.css" rel="stylesheet" type="text/css" />
	
	Note:
	box.css is a small css framework for content structure.
	content.css is for the content blocks.


2. Include JQuery

	<script src="contentbuilder/jquery.min.js" type="text/javascript"></script>  


3. Include ContentBuilder.js plugin and saveimages.js plugin

    <link href="contentbuilder/contentbuilder.css" rel="stylesheet" type="text/css" />
	<script src="contentbuilder/contentbuilder.min.js" type="text/javascript"></script>


	Note:
	ContentBox.js package includes ContentBuilder.min.js which can only be used within ContentBox.js. 
	For more flexibility, you can get ContentBuilder.js full package, available at:	http://innovastudio.com/content-builder.aspx


4. Include ContentBox.js plugin

    <link href="contentbox/contentbox.css" rel="stylesheet" type="text/css" />
	<script src="contentbox/contentbox.min.js" type="text/javascript"></script>


5. Run:

        //Enable editing
        $(".is-wrapper").contentbox({
            coverImageHandler: 'savecover.php', /* for uploading image background */
            largerImageHandler: 'saveimage-large.php', /* for uploading larger image */
            moduleConfig: [{
                "moduleSaveImageHandler": "saveimage-module.php" /* for module purpose image saving (ex. slider usage) */
            }],
            onChange: function () {
                //Auto Save
                var timeoutId;
                clearTimeout(timeoutId);
                timeoutId = setTimeout(function () {
                    save();                    
                }, 1000);
            }
        });

	Or if you are using Bootstrap framework, set framework parameter:

        $(".is-wrapper").contentbox({
            framework: 'bootstrap', 
            ...
        });

	
	NOTE: ContentBox.js runs fully on client side (server independent). The php files used in this example are only for saving image files on the server. You can use other server side platform and create your own handler for saving image files. Saving files on the server is outside the ContentBox.js functions.


	PARAMETERS:
	- framework: framework used (ex. 'bootstrap', 'foundation')
	- coverImageHandler: server side handler for saving background images.
	- largerImageHandler: server side handler for saving larger images.
	- moduleConfig: Containing params for future use. Now only one param is needed ("moduleSaveImageHandler") that is used for slider image upload. 
	

	OTHER USEFUL PARAMETER:
	- customval: Optional custom parameter if you want to pass custom value to the image handler (can be used to specify custom upload folder)


6. To get HTML:

    var sHTML = $('.is-wrapper').data('contentbox').html();


7. For Saving content, all embedded base64 images will be saved first (with the help of a simple handler on the server). 
	Then after all images are saved, get the HTML content and you're ready to submit it to the server for saving purpose (eg. in your database, etc).
	To save all embedded images, use saveimages method. 

    $('.is-wrapper').data('contentbox').saveImages('saveimage.php', function(){

        //Saving images done

        //Get HTML content
        var sHTML = $('.is-wrapper').data('contentbox').html();

        //Get styles (needed by the content). There are two styles:
        var sMainCss = $('.is-wrapper').data('contentbox').mainCss(); //mainCss() returns css that defines typography style for the body/entire page.
        var sSectionCss = $('.is-wrapper').data('contentbox').sectionCss(); //sectionCss returns css that define typography styles for certan section(s) on the page
        
        //Here, you're ready to submit the content and styles to the server for saving.
        //Both content and styles should be saved into your database. 
        //For viewing, content and styles should be displayed together.

    });

	PARAMETERS for saveimages.js plugin:
	- handler: server side handler for saving embedded base64 images.
	- onComplete: Event triggered after all images are saved.	


8. Specify upload folder.

	Open savecover.php & saveimage.php. Change $path variable if needed.

	The default upload folder is "uploads"
		

*** OTHERS ***


ContentBox needs some assets that are located in assets folder.  If you need to change the location, use:

	$(".is-wrapper").contentbox({      
            modulePath: 'assets/modules/',
            assetPath: 'assets/',
            designPath: 'assets/designs/',
            contentStylePath: 'assets/styles/',
            snippetData: 'assets/minimalist-blocks/snippetlist.html',
			...
        });

One category of section templates is a footer section. In case you want to disable this category, use:

	$(".is-wrapper").contentbox({      
            disableStaticSection: true,
			...
        });


*** Language File ***

	With language file you can translate ContentBox.js interface into another language. To include the language file:

		<script src="contentbox/lang/en.js" type="text/javascript"></script>

	Here is the language file content as seen on lang/en.js:

		var _txt = new Array();
		_txt['Bold'] = 'Bold';
		_txt['Italic'] = 'Italic';
		...

	You can create your own language file (by copying/modifying the lang/en.js) and include it on the page where contentbox.js is included.
	This will automatically translate the ContentBox.js interface.


*** Lightbox extension ***

You can disable this if you want.
Lightbox plugin allows users to embed image with option to enlarge image on click. The implementation is as follows: (already implemented in the examples)

1. Include the required lightbox plugin script:
	<link href="assets/scripts/simplelightbox/simplelightbox.css" rel="stylesheet" type="text/css" />
	<script src="assets/scripts/simplelightbox/simple-lightbox.min.js" type="text/javascript"></script>

2. Initiate the lightbox plugin within contentbox using onRender param and specify upload handler for uploading image:

	
        $(".is-wrapper").contentbox({
            ...
            largerImageHandler: 'saveimage-large.php',
            onRender: function () {
                $('a.is-lightbox').simpleLightbox({ closeText: '<i style="font-size:35px" class="icon ion-ios-close-empty"></i>', navText: ['<i class="icon ion-ios-arrow-left"></i>', '<i class="icon ion-ios-arrow-right"></i>'], disableScroll: false });
            }
        });




*** EXAMPLES ***


- example.html (Basic example. Content is saved in browser's Local Storage)

- example.php (Complete example in PHP. Content is saved in session)



*** UPGRADING FROM PREVIOUS VERSION ***

Just copy & replace the following folders:

- assets/
- box/
- contentbox/
- contentbuilder/



*** SUPPORT ***

Email us at: support@innovastudio.com



---- IMPORTANT NOTE : ---- 
1. Custom Development is beyond of our support scope.
Once you get the HTML content, then it is more of to user's custom application (eg. posting it to the server for saving into a file, database, etc).
Programming or server side implementation is beyond of our support scope.

2. Our support doesn't cover custom integration into users' applications. It is users' responsibility.
------------------------------------------
